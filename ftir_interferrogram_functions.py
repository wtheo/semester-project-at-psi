
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 100000

import numpy as np
import os
from tqdm.notebook import tqdm

import scipy
from scipy.constants import c, h, physical_constants
from scipy import signal
from scipy.optimize import curve_fit
from scipy.stats.distributions import t
from scipy.ndimage import gaussian_filter

from lmfit.models import ConstantModel, LorentzianModel, LinearModel
from lmfit.model import save_modelresult, load_modelresult
from lmfit.printfuncs import report_ci

from uncertainties import ufloat
from uncertainties import umath


def removed_internal_reflections(IFG, range = [0,0], smoothing = 30):
    print("removing internal reflections from IFG")
    moving_average = IFG["IFG Value"].rolling(smoothing).mean()
    for i in range(range[0], range[1]):
        IFG.at[i, "IFG Value"] = moving_average.at[i]
    return IFG

def gaussian_func(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.0) / (2 * np.power(sig, 2.0)))

def smooth_spikes_gaussian(IFG, ranger = [0,0], smoothing = 0.5): #1 is maximum smoothing, some parts of ifg will even go to zero 
    print("removing spikes from IFG with gaussian")
    sigma = (ranger[1]-ranger[0])/4
    mu = (ranger[1]+ranger[0])/2
    moving_average = IFG["IFG Value"].rolling(100).mean()
    for i in range(ranger[0], ranger[1]):
        IFG.at[i, "IFG Value"] = moving_average.at[ranger[1]] + (IFG.at[i, "IFG Value"] - moving_average.at[ranger[1]]) * (1.0 - smoothing * gaussian_func(i, mu, sigma))
    return IFG

def auto_remove_internal_reflections(IFG, smoothing = 30, peak_value = 999999.9, spread = 100, left_centreburst = [0,0], right_centreburst = [0,0]):
    moving_average = IFG["IFG Value"].rolling(smoothing).mean()
    for i in range(0,len(IFG)):
        if not(i > left_centreburst[0] and i < left_centreburst[1] or i > right_centreburst[0] and i < right_centreburst[1]):
            if abs(IFG.at[i, "IFG Value"]) > peak_value:
                for j in range(i-spread, i+spread):
                    if j >= smoothing and j < len(IFG):
                        if not(i > left_centreburst[0] and i < left_centreburst[1] or i > right_centreburst[0] and i < right_centreburst[1]):
                            IFG.at[j, "IFG Value"] = moving_average.at[j]
    return IFG
                
        
def split_IFG_forwards_backwards(IFG):
    halfway = int(len(IFG)/2)
    index_left_max = IFG.iloc[:halfway,1].idxmax()
    index_right_max = IFG.iloc[halfway:,1].idxmax()
    true_halfway = int(index_left_max+(index_right_max-index_left_max)/2)
    forwards = IFG.iloc[:true_halfway,:]
    backwards = IFG.iloc[true_halfway:,:]
    backwards = backwards.iloc[::-1]
    forwards.index = range(len(forwards))
    backwards.index = range(len(backwards))
    return forwards, backwards

def set_internal_reflections_to_zero(IFG, ranger = [0,0]):
    average_val = IFG[ranger[0]:ranger[1]]["IFG Value"].mean()
    print(average_val)
    for i in range(ranger[0], ranger[1]):
        IFG.at[i, "IFG Value"] = average_val
    return IFG

"""def add_one_side_of_spectrum_to_other(IFG):
    size = IFG.iloc[:,1].size
    index_max = IFG.iloc[:,1].idxmax()
    if len(IFG.iloc[:index_max,1]) > len(IFG.iloc[index_max:,1]): #if leftside longer than rightside
        IFG = IFG.iloc[::-1]
        IFG.index = range(len(IFG))
    length_left = len(IFG.iloc[:index_max,1])
    section_to_copy = pd.DataFrame(IFG.iloc[(index_max+length_left):,:])
    section_to_copy = section_to_copy.iloc[::-1]
    final_IFG = section_to_copy.append(IFG)
    final_IFG.index = range(len(final_IFG))
    return final_IFG""" #Not sure if these are needed any more

"""def set_one_side_of_spectrum_to_zero(IFG):
    size = IFG.iloc[:,1].size
    index_max = IFG.iloc[:,1].idxmax()
    if len(IFG.iloc[:index_max,1]) > len(IFG.iloc[index_max:,1]): #if leftside longer than rightside
        IFG = IFG.iloc[::-1]
        IFG.index = range(len(IFG))
    length_left = len(IFG.iloc[:index_max,1])
    length_to_add = len(IFG.iloc[(index_max+length_left):,1])
    section_to_copy = pd.DataFrame(IFG.iloc[(index_max+length_left):,:])
    section_to_copy = section_to_copy.iloc[::-1]
    final_IFG = section_to_copy.append(IFG)
    final_IFG.index = range(len(final_IFG))
    return final_IFG""" #Not sure if these are needed any more

"""def smooth_out_MIR_jumps(IFG_ONESIDED, centerburst_halfwidth=5000, average_over=1000):
    print("IFG better be one sided")
    index_max = IFG_ONESIDED.iloc[:,1].idxmax()
    rolling_average_forward = IFG_ONESIDED["IFG Value"].rolling(average_over).mean()
    rolling_average_backwards = IFG_ONESIDED["IFG Value"].iloc[::-1].rolling(average_over).mean()
    rolling_average_forward.at[index_max-centerburst_halfwidth:index_max+centerburst_halfwidth, "IFG Value"] = 0.0
    rolling_average_backwards.at[index_max-centerburst_halfwidth:index_max+centerburst_halfwidth, "IFG Value"] = 0.0
    return rolling_average_forward, rolling_average_backwards""" #Not sure if these are needed any more

def plot_IFGs(IFGs):
    plt.figure(figsize = (10,6))
    for ifg in IFGs:
        plt.plot(ifg["IFG Value"])
    plt.show() 
    
    
######## ACTUAL FT PROCESSING #############


# Zerofilling of the IFG to increase the digital resolution (smoother spectrum)
# Zerofillingfactor needs to be a power of 2.
def ZeroFill_IFG(IFG,ZeroFillFactor=4):
    size = IFG.iloc[:,1].size
    
    i=2
    while (i<size):
        i=i*2 
    i=i*ZeroFillFactor
    ZerosToAdd=i-size
    zeros = np.zeros(ZerosToAdd)


    tmp=pd.DataFrame(np.linspace(0,i,i).astype('int'))
    tmp['tg'] = np.concatenate((IFG.iloc[:,1], zeros), axis=0)
    
    return tmp

# Apodization of IFG using the Blackmann-Harris 3-terms method, full range.
def Apodization(IFG):
    size = IFG.iloc[:,1].size
    window = pd.DataFrame(IFG.iloc[:,0].values,columns=['Index'])
    window['Apodization'] = signal.windows.blackmanharris(size)
    IFG = IFG.reset_index()
    window['Apodization'] = window['Apodization']*IFG['IFG Value']
    return window

#Since we get some drift, we will rotate full IFG so it is parallel to the x axis.
def TiltIFG(IFG):
    #Remove central burst so we get a linear fit
    size = IFG.iloc[:,1].size 
    index_max = IFG.iloc[:,1].idxmax()
    leftIFG = IFG.iloc[0:int(index_max-size/10),:]
    rightIFG = IFG.iloc[int(index_max+size/10):size,:]
    
    Fusion = pd.concat([leftIFG,rightIFG])
    
    linear_model05=np.polyfit(Fusion.iloc[:,0],Fusion.iloc[:,1],5)
    p=np.poly1d(-linear_model05)
    
    ToAdd = p(IFG.iloc[:,0])
    
    IFGTilted = pd.DataFrame(IFG.iloc[:,0])
    
    IFGTilted['Angle'] = IFG.iloc[:,1] + ToAdd + linear_model05[4]
    
    return IFGTilted
    
    
#ToSet Baseline at 0
def BaselineCorrection(IFG):
    IFGcorrected = pd.DataFrame(IFG.iloc[:,0])
    IFGcorrected['Angle'] = IFG.iloc[:,1]-IFG.iloc[:,1].mean()
    return IFGcorrected


#If we want to have a blackmanharris function with smaller FWHM
def Apodization_NotFullIFG(IFG,M=256):
    halfM=int(M/2)
    size = IFG.iloc[:,1].size  

    index_max = IFG.iloc[int(size/2-200):int(size/2+200),1].idxmax()
    
    ToAddTop = np.zeros(index_max-halfM-1)
    ToAddTop_Index = np.linspace(0,index_max-halfM-1,index_max-halfM-1,dtype=int)

    ToAddBottom = np.zeros(size-index_max-halfM+1)
    ToAddBottom_Index = np.linspace(index_max+halfM+1,size-1,size-index_max-halfM+1,dtype=int)
    
    df_Top = pd.DataFrame(ToAddTop_Index,columns=['Index'])
    df_Top['Apodization']=ToAddTop

                             
    df_Bottom = pd.DataFrame(ToAddBottom_Index,columns=['Index'])
    df_Bottom['Apodization']=ToAddBottom
    
    window = pd.DataFrame(np.linspace(index_max-halfM,index_max+halfM,M,dtype=int),columns=['Index'])
    window['Apodization'] = signal.windows.blackmanharris(M)
    
    final = pd.concat([df_Top,window,df_Bottom],ignore_index=True)
    
    return final



#RotationOfIFG
def RotateIFG(IFG):
    
    size = IFG.iloc[:,1].size
    index_max = IFG.iloc[:,1].idxmax()

    LeftSide = pd.DataFrame(IFG.iloc[:index_max,:].values)
    RightSide = pd.DataFrame(IFG.iloc[index_max:,:].values) #include ZPD point
    
    IFGrotated = pd.concat([RightSide,LeftSide],ignore_index=True)
    IFGrotated.iloc[:,0] = np.linspace(0,size-1,size,dtype=int)
    
    return IFGrotated

#IFG needs to be already phase-corrected, zero-filled and normalised
def IFGtoSpectrum(IFG,ZeroFillingFactor=4,n=4.0):    
    FT = scipy.fft.fft(IFG.iloc[:,1].values)
    HeNeLambda = 632.8 #in nm
    HeNeLambda_cm = HeNeLambda *1e-7 #in cm
    retardation_step = n*HeNeLambda_cm
    #to be able to sample correctly wavenumber of 15900 cm-1 = 666 nm,
    #need to have a sampling interval smaller than 666/2 = 333nm, which is the
    # Nyquist "wavelength". This requires to have a sampling interval smaller than
    # 333nm. Sampling half the HeNe wavelength is enough, since 632.8/2 = 316.5 nm$
    #
    
    
    Wavenumbers = scipy.fft.fftfreq(IFG.iloc[:,1].size, d=retardation_step)
    
    #shift zero frequency component at center
    Wavenumbers = scipy.fft.fftshift(Wavenumbers)
    FT = scipy.fft.fftshift(FT)
    
    FT = pd.DataFrame(FT)
    Spectrum = pd.DataFrame(Wavenumbers,columns=['Wavenumbers'])
    Spectrum['Real'] = np.real(FT)
    Spectrum['Imaginary'] = np.imag(FT)
    
    return Spectrum

#Calculate phase at each wavenumber. Since we rotated the IFG, the reference is at ZPD
def PhaseAngle(Spectrum):
    tmp=np.arctan(Spectrum['Imaginary']/Spectrum['Real'])
    PhaseAngle=pd.DataFrame(Spectrum['Wavenumbers'])
    PhaseAngle['Phase']=tmp
    return PhaseAngle

def SinAndCosinePhase(PhaseAngle):
    tmpCos=np.cos(PhaseAngle['Phase'])
    tmpSin=np.sin(PhaseAngle['Phase'])
    SinAndCosinePhase=pd.DataFrame(PhaseAngle['Wavenumbers'])
    SinAndCosinePhase['CosPhase']=tmpCos
    SinAndCosinePhase['SinPhase']=tmpSin
    return SinAndCosinePhase

#SingleBeamPhaseCorrectedSpectrum
def TrueSpectrum(SinAndCosinePhase,Spectrum):
    TrueSpectrum = pd.DataFrame(Spectrum['Wavenumbers'])
    TrueSpectrum['Spectrum'] =  SinAndCosinePhase['CosPhase']*Spectrum['Real'] + \
                                    SinAndCosinePhase['SinPhase']*Spectrum['Imaginary']
    return TrueSpectrum

def MagnitudeSpectrum(Spectrum):
    MagnitudeSpectrum=pd.DataFrame(Spectrum['Wavenumbers'])
    MagnitudeSpectrum['Spectrum'] = np.sqrt(np.abs(Spectrum['Real'])**2 + \
                                                    np.abs(Spectrum['Imaginary'])**2)

    return MagnitudeSpectrum


def IFGtoSpectrumFull(IFG,ZeroFillFactor=4,retardation_step=4.0):
    test0 = TiltIFG(IFG)
    test1 = BaselineCorrection(test0)
    test2 = Apodization(test1)
    test3 = ZeroFill_IFG(test2,ZeroFillFactor=ZeroFillFactor)
    test4 = RotateIFG(test3)
    test5 = IFGtoSpectrum(test4,ZeroFillingFactor=ZeroFillFactor,n=retardation_step)
    test6 = PhaseAngle(test5)
    test7 = SinAndCosinePhase(test6)
    TrueSC= TrueSpectrum(test7,test5)
    MagnitudeSC = MagnitudeSpectrum(test5)
    
    return TrueSC, MagnitudeSC

def IFGtoSpectrumFull_NoBaseline(IFG,ZeroFillFactor=4,retardation_step=4.0):
    test2 = Apodization(IFG)
    test3 = ZeroFill_IFG(test2,ZeroFillFactor=ZeroFillFactor)
    test4 = RotateIFG(test3)
    test5 = IFGtoSpectrum(test4,ZeroFillingFactor=ZeroFillFactor,n=retardation_step)
    test6 = PhaseAngle(test5)
    test7 = SinAndCosinePhase(test6)
    TrueSC= TrueSpectrum(test7,test5)
    MagnitudeSC = MagnitudeSpectrum(test5)
    
    return TrueSC, MagnitudeSC

def IFGDifftoSpectrumFull(IFG_Diff,Ix,Iy,rangeIx=1,rangeIy=1,range_IFG=1,ZeroFillFactor=4,degrees=False):
    IFG_Angle = IFGDifferenceToAngleNoSum(IFG_Diff,Ix,Iy,rangeIx,rangeIy,range_IFG,degrees)
    TrueSC,MagnitudeSC = IFGtoSpectrumFull(IFG_Angle,ZeroFillFactor=ZeroFillFactor)
    
    return TrueSC,MagnitudeSC

def IFGDifftoSpectrumFull_SumAveraged(IFG_Diff,Sum,rangeSum=1,range_IFG=1,ZeroFillFactor=4,degrees=False):
    IFG_Angle = IFGDifferenceToAngleSumAveraged(IFG_Diff,Sum,rangeSum,range_IFG,degrees)
    TrueSC,MagnitudeSC = IFGtoSpectrumFull(IFG_Angle,ZeroFillFactor=ZeroFillFactor)
    
    return TrueSC,MagnitudeSC


    
    

######## FOR SINGLE SIDED SPECTRA: #######

def single_sided_phase_correction_data(IFG):
    size = IFG.iloc[:,1].size
    index_max = IFG.iloc[:,1].idxmax()
    left_side_size = IFG.iloc[:index_max,1].size
    right_side_size = IFG.iloc[index_max:,1].size
    if right_side_size < left_side_size:
        print("right side shorter than left side, something is WRONG mate, u did it wrong way round somewhere")
        return
    zeros = np.zeros(size)
    # take a power of 2 points centred around zpd, maximum size as the smallest side of IFG
    small_side=2
    while (small_side<left_side_size):
        if small_side*2 > left_side_size:
            break
        small_side=small_side*2
    small_IFG = IFG.iloc[index_max-int(small_side/2):index_max+int(small_side/2),:]
    small_IFG = Apodization(small_IFG)
    small_IFG = RotateIFG(small_IFG)
    small_spectra = IFGtoSpectrum(small_IFG,ZeroFillingFactor=1,n=0.5)
    power_small_spectrum = MagnitudeSpectrum(small_spectra)
    small_spectra['CosPhase'] = power_small_spectrum['Spectrum'] * small_spectra['Real']
    small_spectra['SinPhase'] = power_small_spectrum['Spectrum'] * small_spectra['Imaginary']
    
    return small_spectra


def ramp_single_sided_spectra(IFG):
    size = IFG.iloc[:,1].size
    index_max = IFG.iloc[:,1].idxmax()
    
    print("index_max ramp:" + str(index_max))
    
    first_half_triangle = np.bartlett(index_max*4)[:(index_max*2)]
    second_half_triangle = np.ones(size-(index_max*2))
    
    final_ramp = np.append(first_half_triangle, second_half_triangle)
    
    IFG['Angle'] = IFG['Angle'] * final_ramp
    
    print(len(final_ramp))
    print(len(IFG['Angle']))
    
    return IFG
    

def apodise_single_sided_spectra(IFG):
    size = IFG.iloc[:,1].size
    index_max = IFG.iloc[:,1].idxmax()
    
    print("index_max apod:" + str(index_max))
    
    first_half_blackman = np.blackman((size-(index_max))*2)[(size-(index_max))-index_max:(size-(index_max))]
    second_half_blackman = np.blackman((size-(index_max))*2)[(size-(index_max)):]
    
    final_blackman = np.append(first_half_blackman, second_half_blackman)
    
    #plt.figure()
    #plt.plot(range(len(final_triangle)),final_triangle)
    #plt.plot(range(len(final_blackman)),final_blackman)
    #plt.show()
    
    IFG['Angle'] = IFG['Angle'] * final_blackman
    
    return IFG

def single_sided_IFG_to_spectrum_full(IFG,ZeroFillFactor=2,retardation_step=0.5):
    phase_correction_data = single_sided_phase_correction_data(IFG)
    var_IFG = BaselineCorrection(IFG)
    #var_IFG = ramp_single_sided_spectra(var_IFG)
    var_IFG = apodise_single_sided_spectra(var_IFG)
    var_IFG = ZeroFill_IFG(var_IFG,ZeroFillFactor=ZeroFillFactor)
    var_IFG = RotateIFG(var_IFG)
    var_IFG = IFGtoSpectrum(var_IFG,ZeroFillingFactor=ZeroFillFactor,n=retardation_step)
    TrueSC = TrueSpectrum(phase_correction_data,var_IFG)
    MagnitudeSC = MagnitudeSpectrum(var_IFG)
    
    return TrueSC, MagnitudeSC
    
    
def average_IFGs_from_folder(path):
    directory = os.fsencode(path)

    counter = 0

    averaged_IFG = []

    for file in tqdm(os.listdir(directory)):
        filename = os.fsdecode(file)

        if filename.endswith(".txt"):
            counter = counter + 1
            IFG_full = pd.read_csv(path+"/"+filename, sep="\t", header=None, names=["Step", "IFG Value"])
            #IFG_full = auto_remove_internal_reflections(IFG_full, smoothing = 30, peak_value = 0.0009, spread = 5, left_centreburst = left_centreburst, right_centreburst = right_centreburst)

            if len(averaged_IFG) == 0:
                averaged_IFG = IFG_full
            else:
                averaged_IFG["IFG Value"] = averaged_IFG["IFG Value"] + IFG_full["IFG Value"]


        else:
            print("file does not end in .txt: " + filename)
       
    averaged_IFG["IFG Value"] = averaged_IFG["IFG Value"]/counter
    print(str(counter) + " data sets processed")
    return averaged_IFG


# removes annoying side peaks in interferogram by clipping data to cutoffLevel above and below package averages
def RemovedInternalReflections(IFG, entireRegion = False, packageSize = 500, tolerance = 98, theta = 1.2, peaktheta = 3):
    
    # divides data into packages of packageSize data points
    numPacks = int(np.floor(len(IFG)/packageSize)) # last couple of data entries are discarded as nothing interesting happens there anyways
    noiseLevels = []
    packageMeans = []
    
    # iterates through packages and determines data spread within package. I call this data spread the 'noise level'
    for i in tqdm(range(numPacks), desc = "computing noise levels"):
        package = IFG.loc[packageSize*i:packageSize*(i+1), :]
        packageMax = package.max()['IFG Value']
        packageMin = package.min()['IFG Value']
        packageNoise = packageMax - packageMin
        noiseLevels.append(packageNoise)
        packageMeans.append(package.mean()['IFG Value'])
        
    packageNoiseLevels = noiseLevels.copy()
    # sorts noise levels of packages
    noiseLevels.sort()
    averageNoiseLevel = sum(noiseLevels) / len(noiseLevels)
    
    print("Average noise level = ", averageNoiseLevel)
    
    # tolerance% of package noise levels lie below cutoff level. Clips all data outside central peaks that lie above this noise level
    cutoffLevel = 0.5*noiseLevels[int(np.floor(tolerance*numPacks/100))]
    print(tolerance, "% of data noise below: ", cutoffLevel)
    
    cutoffLevel *= theta
    print("Cutoff at: ", cutoffLevel)
    cutoffLevel2 = peaktheta * cutoffLevel
    print("Peak tolerance at:", cutoffLevel2)
    
    # we want the package average to not only be the average of the package but also the packages 2 left and 2 right of the package
    packageMeanMeans = np.array(packageMeans)
    packageMeanMeans[0] = (packageMeans[0] + packageMeans[1] + packageMeans[2])/3
    packageMeanMeans[1] = (packageMeans[0] + packageMeans[1] + packageMeans[2] + packageMeans[3] )/4
    packageMeanMeans[-1] = (packageMeans[-3] + packageMeans[-2] + packageMeans[-1])/3
    packageMeanMeans[-2] = (packageMeans[-4] + packageMeans[-3] + packageMeans[-2] + packageMeans[-1])/4
    packageMeanMeans[2:-3] = np.array(packageMeans)[0:-5] + np.array(packageMeans)[1:-4] + np.array(packageMeans)[2:-3] + np.array(packageMeans)[3:-2] + np.array(packageMeans)[4:-1]
    packageMeanMeans[2:-3] /= 5
    packageMeans = packageMeanMeans.tolist()
    
    # defines smoothing region
    if (entireRegion):
        leftBound = numPacks/2
        rightBound = numPacks/2+1
        # loading bar
        pbar = tqdm(desc = "clipping", total = np.floor(numPacks))
    else: 
        leftBound = numPacks/16
        rightBound = numPacks/16*15    
        # loading bar
        pbar = tqdm(desc = "clipping", total = np.floor(numPacks/8)+2)
        
    ########################## left half ##########################
    
    # gets position and package number for left peak 
    leftPeakPos = IFG.loc[0:len(IFG)/2-1,:].idxmax('rows')["IFG Value"]
    leftPeakPackNum = int(np.floor(leftPeakPos/packageSize))
    
    # clip all data left of left peak 
    j = leftPeakPackNum
            
    while (packageNoiseLevels[j] > cutoffLevel2):
        j-=1
        pbar.update(1)
        if(j <= 0):
            print("Peak region too large. Set higher peakTheta.")
            break
    
    while (j >= 0):
        lowerBound = packageMeans[j] - cutoffLevel
        upperBound = packageMeans[j] + cutoffLevel
        IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'] = IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'].clip(lower = lowerBound, upper = upperBound)
        j-=1
        pbar.update(1)
        
    # clip data on the right of left peak until middle 
    j = leftPeakPackNum
    
    while (packageNoiseLevels[j] > cutoffLevel2):
        j+=1        
        pbar.update(1)
        if(j >= leftBound):
            print("Peak regions extends to right of leftBound. Either set entire region to True or increase peakTheta.")
            break
    
    while (j <= leftBound):
        lowerBound = packageMeans[j] - cutoffLevel
        upperBound = packageMeans[j] + cutoffLevel
        IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'] = IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'].clip(lower = lowerBound, upper = upperBound)
        j+=1
        pbar.update(1)
    
    ########################## right half ##########################

    
    # gets position and package number for right peak
    rightPeakPos = IFG.loc[len(IFG)/2:len(IFG),:].idxmax('rows')["IFG Value"]
    rightPeakPackNum = int(np.floor(rightPeakPos/packageSize))
    
    # clip data on the left of right peak until middle 
    j = rightPeakPackNum
    
    while (packageNoiseLevels[j] > cutoffLevel2):
        j-=1
        pbar.update(1)
        if(j <= rightBound):
            print("Peak regions extends to left of rightBound. Either set entire region to True or increase peakTheta.")
            break
    
    while (j >= rightBound):
        lowerBound = packageMeans[j] - cutoffLevel
        upperBound = packageMeans[j] + cutoffLevel
        IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'] = IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'].clip(lower = lowerBound, upper = upperBound)
        j-=1
        pbar.update(1)
    
    # clip data on the right of right peak 
    j = rightPeakPackNum
    
    numPacksMinusTwo = numPacks - 2
    
    while (packageNoiseLevels[j] > cutoffLevel2):
        j+=1
        pbar.update(1)
        if(j >= numPacksMinusTwo):
            print("Peak region too large. Set higher peakTheta.")
            break
    
    while (j <= numPacksMinusTwo):
        lowerBound = packageMeans[j] - cutoffLevel
        upperBound = packageMeans[j] + cutoffLevel
        IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'] = IFG.loc[packageSize*j:packageSize*(j+1), 'IFG Value'].clip(lower = lowerBound, upper = upperBound)
        j+=1
        pbar.update(1)
        
    return IFG

# alternative approach to removing side peaks using gaussian filter
def process_df(df, A, B):
    # Center the values
    df['IFG Value'] = df['IFG Value'] - df['IFG Value'].mean()

    # Split data into two halves
    first_half = df[:len(df)//2]
    second_half = df[len(df)//2:]
   
    # Find absolute maximal values in both halves
    Amax = first_half['IFG Value'].abs().max()
    Amax_index = first_half['IFG Value'].abs().idxmax()
    Bmax = second_half['IFG Value'].abs().max()
    Bmax_index = second_half['IFG Value'].abs().idxmax()
   
    # Calculate moving average of standard deviation
    moving_std = df['IFG Value'].rolling(window=500).std()
   
    # Calculate moving average
    moving_mean = df['IFG Value'].rolling(window=500).mean()
   
    # Start index
    start_index = 0

    while start_index < len(df):
        # Find peak that is 10% the value of Amax and not within 500 steps of Amax_index or Bmax_index
        poop = None
        for index in range(start_index, len(df)):
            if abs(index - Amax_index) > 3000 and abs(index - Bmax_index) > 3000 and df.loc[index, 'IFG Value'] >= A * moving_std.loc[index] + moving_mean.loc[index]:
                poop = index
                break

        # If no peak is found, stop
        if poop is None:
            break

        # Scan the next 500 steps of the small region to find the maximal value of that small region
        small_region = df[poop-10:poop+2000]
        max_val = small_region['IFG Value'].max()
        max_index = small_region['IFG Value'].idxmax()

        Y = int(max_index - poop) * B + 100
        # Find the mean of the Y steps around the maximal value
        mean_val = df[max_index-Y:max_index+Y]['IFG Value'].mean()
        std_val = df[max_index-Y:max_index+Y]['IFG Value'].std()

        # Create a gaussian filter centered at the maximal value of the small region with standard deviation Y
        x = np.arange(-Y, Y + 1)
        gaussianF = np.exp(-x**2 / (2*Y**2))
        gaussianF = 1 - gaussianF / gaussianF.max()
        
        print(gaussianF.size())
        print(df.loc[max_index-Y:max_index+Y, 'IFG Value'].size())

        # Apply the gaussian filter
        df.loc[max_index-Y:max_index+Y, 'IFG Value'] = gaussianF * df.loc[max_index-Y:max_index+Y, 'IFG Value'] + (1-gaussianF)*mean_val

        # Update start index
        start_index = max_index + Y

    return df

# input: pandas array IFG, tuple ranger
# returns: IFG with values in interval between rangers averaged out --> removes spikes from internal reflections
def removed_internal_reflections(IFG, ranger = [0,0]):
    average_val = IFG[ranger[0]:ranger[1]]["IFG Value"].mean()
    for i in range(ranger[0], ranger[1]):
        IFG.at[i, "IFG Value"] = average_val
    return IFG

# divides internal reflection peaks with a gaussian to smoothen IFG. Bascially "process_df" function except you have to define smoothing regions manually and it actually works
def gaussJujitsu(IFG, ranger = [0,0]):
    
    averageVal = IFG[ranger[0]:ranger[1]]["IFG Value"].mean()
    snippetY = np.array(IFG[ranger[0]:ranger[1]]["IFG Value"].values.tolist()) - averageVal
    snippetX = np.arange(snippetY.size)
    
    x0 = np.argmax(snippetY)
    sigma0 = (ranger[1] - ranger[0])/2
    y0 = 0
    a0 = snippetY.max()
    
    p = [a0, x0, sigma0, y0]
    gaussY = gaussian(snippetX, *p)
    snippetY = snippetY * (1-gaussY/gaussY.max()) + averageVal
    
    IFG[ranger[0]:ranger[1]]["IFG Value"] = snippetY
    
    return IFG


# calculates energy in eV from wavenumbers in cm^-1
def energy(wavenumber):
    return 100*wavenumber*c*h/physical_constants["electron volt"][0]

# calculates wavenumber from energy
def wavenumber(energy):
    return energy*physical_constants["electron volt"][0]/100/c/h

# splits and averages interferograms    
def splitAndFold(IFG):
    forward, backward = split_IFG_forwards_backwards(IFG)
    folded = forward
    folded.loc[:,"IFG Value"] += backward.loc[:,"IFG Value"]
    folded.loc[:,"IFG Value"] /= 2
    return folded

# rotates IFG such that peak sits at 0th and -1th index in array
def rotate(yvals):
    
    size = yvals.size
    index_max = yvals.argmax()
    LeftSide = np.array(yvals[:index_max])
    RightSide = np.array(yvals[index_max:]) #include ZPD point
    IFGrotated = np.concatenate((RightSide,LeftSide), axis = 0)
    
    return IFGrotated

# gets fourier transform and gets area under the curve
def getArea(data):
    
    xvalues = np.array(data["Step"].values.tolist())
    yvalues = np.array(data["IFG Value"].values.tolist())
    
    # filters NaN entries in array and interpolates
    yi = np.arange(len(yvalues))
    mask = np.isfinite(yvalues)
    yfiltered = np.interp(yi, yi[mask], yvalues[mask])
    
    # use rotate to get nice fft
    yfiltered = rotate(yfiltered)
      
    yft = scipy.fft.fft(yfiltered)
    xft = scipy.fft.fftfreq(xvalues.size)
    xft = scipy.fft.fftshift(xft)
    
    # numerical integration of spectrum
    area = np.abs(np.trapz(yft, xft))
    return area


def gaussian(x, a, x0, sigma, y0):
    return a*np.exp(-(x-x0)**2/(2*sigma**2)) + y0

def lorentzian(x, a, x0, gamma, y0):
    return a*gamma**2/(((x-x0)**2)+gamma**2) + y0

def voigt(x, a, x0, sigma, gamma, y0):
    return a*scipy.special.voigt_profile(x-x0, sigma, gamma)+y0


# does gaussian, lorentzian or voigt fit between startEnergy and endEnergy to find peak value and fwhm
# plots stuff
def getPeakVals(energies, absorpSpectrum, startEnergy, endEnergy, fitType, peakName, fileName, resultspath):
    # defines fitting region
    startIndex = int(np.floor(energies.size*(startEnergy-energies.values[0])/(energies.values[-1]-energies.values[0])))
    endIndex   = int(np.floor(energies.size*(endEnergy-energies.values[0])/(energies.values[-1]-energies.values[0])))

    energyRange   = np.array(      energies[startIndex:endIndex].values.tolist())
    spectrumRange = np.array(absorpSpectrum[startIndex:endIndex].values.tolist())
    
    # starting parameters for curve fitting
    mu0 = energyRange[np.argmax(spectrumRange)]
    sigma0 = np.sqrt(np.sum(spectrumRange*(energyRange-mu0)**2)/np.sum(spectrumRange))
    y0 = spectrumRange[0]
    a0 = spectrumRange.max() - y0
    ymin = spectrumRange.min()
    
    if (fitType == "gaussian"):
        fitLabel = "Gaussian fit"
        popt, pcov = scipy.optimize.curve_fit(gaussian, energyRange, spectrumRange, p0 = [a0, mu0, sigma0, y0], bounds = (0, np.inf))
        
        yfit = gaussian(energies, *popt)
        perr = np.sqrt(np.diag(pcov))
        
        energy = ufloat(popt[1], perr[1])
        FWHM = 2.355*ufloat(popt[2], perr[2])
        height = ufloat(popt[0], perr[0])
        ground = popt[3]
        
    elif (fitType == "lorentzian"): 
        fitLabel = "Lorentzian fit"
        popt, pcov = scipy.optimize.curve_fit(lorentzian, energyRange, spectrumRange, p0 = [a0, mu0, sigma0, y0], bounds = (0, np.inf))
        
        yfit = lorentzian(energies, *popt)
        perr = np.sqrt(np.diag(pcov))
        
        energy = ufloat(popt[1], perr[1])
        FWHM = 2*ufloat(popt[2], perr[2])
        height = ufloat(popt[0], perr[0])
        ground = popt[3]
        
    elif (fitType == "voigt"):
        fitLabel = "Voigt profile"
        poptG, pcovG = scipy.optimize.curve_fit(gaussian, energyRange, spectrumRange, p0 = [a0, mu0, sigma0, y0], bounds = (0, np.inf))
        popt, pcov = scipy.optimize.curve_fit(voigt, energyRange, spectrumRange, p0 = [poptG[0]*poptG[2]*np.sqrt(2*np.pi), poptG[1], poptG[2]/1.6, poptG[2]/1.6, poptG[3]], bounds = (0, np.inf), maxfev = 10**8)
        
        yfit = voigt(energies, *popt)
        perr = np.sqrt(np.diag(pcov))
        
        fG = 2*ufloat(popt[2], perr[2])
        fL = 2.355*ufloat(popt[3], perr[3])
        energy = ufloat(popt[1], perr[1])
        FWHM = 0.5346*fL + umath.sqrt(0.2166*fL**2+fG**2)
        height = ufloat(voigt(0, popt[0], 0, popt[2], popt[3], 0), 0)
        ground = popt[4]
        
    else:
        print("Error, function type not defined. Allowed options: gaussian, lorentzian")
        return 0, 0

    print("Parameters = ", popt)
    print("Errors = ", perr)
    print(peakName, " transition Energy = ", 1000*energy, " meV, FWHM = ", FWHM*1000, "meV, height =", height)
    
    figureName  = fileName + fitType + "TransitionPeak.png"
    figureName2 = fileName + fitType + "transitionPeak.eps"
    saveFigTo   = os.path.join(resultspath, figureName)
    saveFigTo2  = os.path.join(resultspath, figureName2)
    
    plt.figure()
    plt.plot(1000*energies, absorpSpectrum, label = "Measurements")
    plt.plot(1000*energies, yfit, label = fitLabel)
    plt.xlim(1000*(energy.n-4*FWHM.n), 1000*(energy.n+4*FWHM.n))
    plt.ylim(ground-min(0.04, a0), y0+1.2*a0)
    plt.legend()
    #plt.title(peakName + " transition peak")
    plt.grid()
    plt.xlabel("Energy in meV")
    plt.ylabel("Absorbance")
    plt.savefig(saveFigTo)
    plt.savefig(saveFigTo2)
    plt.show()
    
    return energy, FWHM, height  



